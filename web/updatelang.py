#! /usr/bin/env python3
# -*- coding: utf8 -*-
#
# Copyright (C) 2018  TMW-2
# Author: Jesusalva

import os

defaultLang = "en"
rootPath = "../../web/"

langs=[]
files={}
originals={}
tm=["","\n"]

def invalidline(line, filx="none"):
    return (line.startswith("include ") or
            line.startswith("<?php") or
            line.startswith("?>") or
            "<div " in line or
            "</div>" in line or
            "iframe" in line or
            "header" in filx or
            (line.startswith("    <p>") and line.endswith("<p>\n")) or
            (line.startswith("    </p>") and line.endswith("</p>\n")) or
            (line.startswith("    <pre><code>") and line.endswith("<code>\n")) or
            (line.startswith("    </code></pre>") and line.endswith("</pre>\n")) or
            line.replace('\n','') == "")

# Populates the basic arrays
def populate():
    # Populate langs
    o=open("langs.txt", "r")
    for i in o:
        langs.append(i.replace('\n',''))
    o.close()

    # Populate files
    for i in os.listdir(rootPath):
        if i.endswith(".php.en"):
            print("Valid file: "+str(i))
            files[i]={}
            for x in langs:
                files[i][x]={}

    #print(str(files))


# Creates/Loads stuff
def genreadPoFile():
    for a in langs:
        print("Reading translations for "+a)
        for i in files:
            readPoFile(i,a)

def generatePoFiles():
    global tm
    context=langs
    context.append('en')
    for a in langs:
        print("Updating po file for "+a)
        xrv=open("po/"+a+".po", "w")
        # Prints header
        xrv.write('\
# Copyright (C) 2018 TMW2\n\
#\n\
\n\
msgid ""\n\
msgstr ""\n\
"Project-Id-Version: TMW2\\n"\n\
"MIME-Version: 1.0\\n"\n\
"Content-Type: text/plain; charset=UTF-8\\n"\n\
"Content-Transfer-Encoding: 8bit\\n"\n\
\n\
')
        xrv.close()

        tm=[""]
        for i in files:
            writePoFile(i,a)
    context.remove('en')

def generateLocal():
    for a in langs:
        print("generating local file for "+a)
        for i in files:
            writeLocal(i,a)



# Write po files. TODO: WARNING: Some translations are getting overriden!
def writePoFile(nf,lg):
    # Translation Memory, to prevent duplicates
    global tm

    a=open("po/"+lg+".po", "a")
    f=open(rootPath+nf, "r")

    for line in f:
        if line in tm or invalidline(line, nf):
            continue
        else:
            tm.append(line)

        a.write("#, no-c-format\n")
        a.write('msgid "%s"\n' % line.replace('\n', '').replace('"','\\"'))
        try:
            if files[nf][lg][line.replace('\n', '')] != line.replace('\n', '') and files[nf][lg][line.replace('\n', '')] != "":
                a.write('msgstr "%s"\n' % files[nf][lg][line.replace('\n', '')].replace('\n','').replace('"','\\"'))
            else:
                a.write('msgstr "%s"\n' % "")
        except KeyError:
                a.write('msgstr "%s"\n' % "")

        a.write('\n')

    a.close()


# Reads Po Files
def readPoFile(nf, lg):
    try:
        a=open("po/"+lg+".po", "r")
    except:
        a=open("po/"+lg+".po", "w")
        a.close()
        a=open("po/"+lg+".po", "r")

    ctx=""
    for line in a:
        if "msgid " in line:
            ctx=line.replace('"\n','').replace('\\"','"').replace('msgid "', "")
        if "msgstr " in line and ctx != "":
            if line != 'msgstr ""\n':
                files[nf][lg][ctx]=line.replace('"\n', '').replace('\\"','"').replace('msgstr "', "")
            else:
                files[nf][lg][ctx]=ctx
            ctx=""

    a.close()

def writeLocal(nf, lg):
    a=open(rootPath+nf[:-2]+lg, 'w')
    b=open(rootPath+nf, 'r')

    for line in b:
        try:
            a.write(files[nf][lg][line.replace('\n', '')])
            a.write('\n')
        except KeyError:
            a.write(line)

    b.close()
    a.close()

# Mainframe: populate arrays
populate()

# Mainframe: handle PO files
genreadPoFile()
generatePoFiles()
generateLocal()

