#!/usr/bin/python2

print("Please be sure the legacy database is on exp.txt,")
print("And you have a template ready. It'll output to EDITME.")
print("Max value and races will be disregarded.")

f=open("exp.txt", "r")
o=open("EDITME", "w")

for a in f:
    b=a.split(',')
    cnt=0
    for x in b:
        if cnt not in [0, 1]:
            o.write(x+',')
        cnt+=1
        if (cnt-2) % 10 == 0:
            o.write('\n')

f.close()
o.close()
